import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:page_transition/page_transition.dart';
import 'package:proyecto_wtm2019/pantalla.dart';

void main(){
  //TODO: tratar de fixear el problema del color del texto en el bar navigation
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Colors.white
      )
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'wtm demo',
      home: Cuerpo(),
    );
  }
}

class Cuerpo extends StatefulWidget {
  @override
  _CuerpoState createState() => _CuerpoState();
}

class _CuerpoState extends State<Cuerpo> with TickerProviderStateMixin{

  TabController _controller;
  int _index;

  @override
  void initState() {
    // TODO: implement initState
    _index = 0;
    _controller =TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        title:Icon(Icons.style,color: Colors.black,),
        centerTitle: true,
      ),
      bottomNavigationBar: BottomNavyBar(
          items: [
            BottomNavyBarItem(
              icon: Icon(Icons.home),
              title: Text('Home',style: TextStyle(color:Colors.black)),
              activeColor: Colors.black12,
            ),
            BottomNavyBarItem(
                icon: Icon(Icons.search),
                title: Text('Search',style: TextStyle(color:Colors.black)),
                activeColor: Colors.black12
            ),
            BottomNavyBarItem(
                icon: Icon(Icons.favorite),
                title: Text('Favourite',style: TextStyle(color:Colors.black),),
                activeColor: Colors.black12
            ),
            BottomNavyBarItem(
                icon: Icon(Icons.shopping_cart),
                title: Text('Shop',style: TextStyle(color:Colors.black)),
                activeColor: Colors.black12
            ),
          ],
          onItemSelected: (index) => setState(() {
            _index = index;
            _controller.animateTo(_index);
          })),
      body: Tabs(controller: this._controller,)
    );
  }
}

class Tabs extends StatefulWidget {

  TabController controller;
  Tabs({this.controller});

  @override
  _TabsState createState() => _TabsState(controller: controller);
}

class _TabsState extends State<Tabs> {

  TabController controller;
  _TabsState({this.controller});

  @override
  Widget build(BuildContext context) {
    return TabBarView(
      physics: NeverScrollableScrollPhysics(),
      controller: controller,
      children: <Widget>[
        Home(),
        Container(color: Colors.green,),
        Container(color: Colors.pink,),
        Container(color:  Colors.blue)
      ],
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    const colorApagado=50;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.white,
      child: ListView(
        children: <Widget>[
          elemento(
            color: Colors.green[colorApagado],
            nombre: 'AIR MAX',
            numero: 270,
            caracte: '"Gold"',
            photo: "assets/spotify.png",
            precioD: "\$349",
            precioO: "\$199",
            colorF: Colors.green[colorApagado+50]
          ),
          elemento(
            color: Colors.green[colorApagado],
            nombre: 'AIR MAX',
            numero: 270,
            caracte: '"Gold"',
            photo: "assets/spotify.png",
            precioD: "\$349",
            precioO: "\$199",
            colorF: Colors.green[colorApagado+50],
          ),
          elemento(
            color: Colors.green[colorApagado],
            nombre: 'AIR MAX',
            numero: 270,
            caracte: '"Gold"',
            photo: "assets/spotify.png",
            precioD: "\$349",
            precioO: "\$199",
            colorF: Colors.green[colorApagado+50]
          ),
        ],
      )
    );
  }
}

class elemento extends StatefulWidget {

  final String precioO;
  final String precioD;
  final String nombre;
  final int numero;
  final String caracte;
  final String photo;
  final Color color;
  final Color colorF;

  elemento({
    this.precioD,this.precioO,this.color,this.nombre,this.photo,this.colorF,this.caracte,this.numero
  });

  @override
  _elementoState createState() => _elementoState(color: color,caracte: caracte,colorF: colorF,nombre: nombre,photo: photo,numero: numero,precioD: precioD,precioO: precioO);
}

class _elementoState extends State<elemento> {

  final String precioO;
  final String precioD;
  final String nombre;
  final int numero;
  final String caracte;
  final String photo;
  final Color color;
  final Color colorF;

  Function _funcion;

  initState(){
    _funcion = (){};
  }

  _elementoState({
    this.precioD,this.precioO,this.color,this.nombre,this.photo,this.colorF,this.caracte,this.numero
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        decoration: BoxDecoration(
            color:  color,
            borderRadius: BorderRadius.circular(16.0)
        ),
        margin: EdgeInsets.only(bottom: 16.0),
        height: 300,
        width: 300,
        child: Column(
          children: <Widget>[
            Container(
                height: 300/1.5,
                child: ContenedorImagen(photo: photo,color: colorF,numero:numero),),
            Container(
                margin: EdgeInsets.only(top: 12.0,bottom: 8.0),
                child: Center(child: Text(
                  nombre+" "+numero.toString()+" "+caracte
                  ,style: TextStyle(color: Colors.black,fontWeight: FontWeight.w300),),)),
            ContenedorPrecios(precioO: precioO,precioD: precioD,)
          ],
        ),
      ),
      onTap: (){
        Navigator.push(
            context, PageTransition(
            type: PageTransitionType.fade,
            duration: Duration(milliseconds: 300),
            child: Pantalla()));
      },
    );
  }
}

class ContenedorImagen extends StatelessWidget {

  final Color color;
  final String photo;
  final int numero;

  ContenedorImagen({this.photo,this.color,this.numero});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          //padding: EdgeInsets.only(top: 24.0),
          child: Text(numero.toString(),
            style: TextStyle(fontSize: 120.0,color: color,fontWeight: FontWeight.w800),),
        ),
        Positioned(
          top: 0.0,
          right: 0.0,
          left: 10.0,
          child: Container(
          padding: EdgeInsets.only(top: 50.0),
          /*child: Hero(
            tag: 'imagenDecorado',
            child: GestureDetector(
              onTap: (){
                Navigator.push(
                    context, PageTransition(
                    type: PageTransitionType.fade,
                    duration: Duration(milliseconds: 500),
                    child: Pantalla()));
              },*/
              child: Image.asset(photo,height: 150.0,width: 150.0,),
            //)
          //),
        ))
      ],
    );
  }
}

class ContenedorPrecios extends StatelessWidget {

  final String precioO;
  final String precioD;

  ContenedorPrecios({this.precioO,this.precioD});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 12.0,bottom: 12.0,right: 12.0),
      child: Row(
        children: <Widget>[
          IconButton(icon: Icon(Icons.favorite_border,size: 30.0,color: Colors.black54,),
              onPressed: (){}),
          Expanded(
            child: Container(
              child: Column(
                children: <Widget>[
                  Text(precioO,style: TextStyle(color: Colors.red,decoration: TextDecoration.lineThrough),),
                  Padding(padding: EdgeInsets.symmetric(vertical: 2.0),),
                  Text(precioD,style: TextStyle(color: Colors.black,fontSize: 25.0),)
                ],
              ),
            ),
          ),
          IconButton(icon: Icon(Icons.add_shopping_cart,size: 30.0,color: Colors.black54,),
              onPressed: (){}),
        ],
      ),
    );
  }
}



