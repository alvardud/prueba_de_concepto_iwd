import 'package:flutter/material.dart';

class Pantalla extends StatefulWidget{
  @override
  _pantallaState createState() => _pantallaState();
}

class _pantallaState extends State<Pantalla> with SingleTickerProviderStateMixin{

  double pad;
  Animation<double> animation;
  AnimationController animationController;

  initState(){
    super.initState();
    pad=200.0;
    animationController = AnimationController(vsync: this,duration: Duration(milliseconds: 500));
    animation = Tween<double>(begin: pad,end: 0.0).animate(animationController);
    
    animation.addListener((){
      setState(() {
      });
    });
    
    animation.addStatusListener((status){});
    
    animationController.forward();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios,color: Colors.black,), onPressed: (){
              //TODO: regresar
          Navigator.pop(context);
        }),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title:Icon(Icons.style,color: Colors.black,),
        centerTitle: true,
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            ContenedorImagenes(),
            Container(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.only(left: animation.value),
                child: ContenedorCaracteristicas(),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ContenedorImagenes extends StatelessWidget {

  final Color color=Colors.green[100];
  final String photo='assets/spotify.png';
  final int numero=270;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/2,
      color: Colors.green[50],
        child: Stack(
          children: <Widget>[
            Padding(padding: EdgeInsets.only(left: 75),
              child: Container(
                //padding: EdgeInsets.only(top: 24.0),
                child: Text(numero.toString(),
                  style: TextStyle(fontSize: 120.0,color: color,fontWeight: FontWeight.w800),),
              ),
            ),
            Positioned(
                top: 0.0,
                right: 0.0,
                left: 10.0,
                child: Container(
                  padding: EdgeInsets.only(top: 50.0),
                  //child: Hero(
                    //tag: 'imagenDecorado',
                    child: Image.asset(photo,height: 150.0,width: 150.0,),
                  //),
                ))
          ],
        ),
    );
  }
}

class ContenedorCaracteristicas extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: (MediaQuery.of(context).size.height/2)-80,
      padding: EdgeInsets.all(12.0),
      child: Container(
        child: ListView(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(child: Text(
                  'AIR MAX 270\n"Gold"',
                  style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.w700,letterSpacing: 4.0),
                ),),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text('\$349',style: TextStyle(color: Colors.red,decoration: TextDecoration.lineThrough),),
                    Text('\$199',style: TextStyle(color: Colors.black,fontSize: 20.0),),
                  ],
                ),
              ],
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 12.0),),
            Text('AVAILABLE SIZES',
              style: TextStyle(color: Colors.black38,fontWeight: FontWeight.w700),),
            Padding(padding: EdgeInsets.symmetric(vertical: 12.0),),
            Row(
              children: <Widget>[
                Expanded(child: Text('US 6',style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold),),),
                Expanded(child: Text('US 7',style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold),),),
                Expanded(child: Text('US 9',style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold),),),
                Expanded(child: Text('US 9.5',style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold),),),
                Expanded(child: Text('US 10',style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold),),),
              ],
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 16.0),),
            Text('DESCRIPTION',
              style: TextStyle(color: Colors.black38,fontWeight: FontWeight.w700),),
            Padding(padding: EdgeInsets.symmetric(vertical: 8.0),),
            Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum dolor justo, ornare at posuere eu, tristique finibus sapien. Aliquam orci magna, finibus semper nunc in, efficitur posuere velit. Mauris semper magna orci, a pulvinar ex pharetra eget. Mauris convallis pharetra lorem, consectetur vulputate nulla tempus vitae. Curabitur ex mi, elementum ac sagittis vel, tincidunt vel ex. Nam vel justo vel ante faucibus pellentesque sed sit amet dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vehicula ante et efficitur pulvinar. Integer sollicitudin cursus metus hendrerit sollicitudin. Duis sit amet faucibus ligula.')
          ],
        ),
      ),
    );
  }
}
